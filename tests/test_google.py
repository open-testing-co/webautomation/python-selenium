"""
These tests cover Google searches.
"""
from pages.search import GoogleSearchPage
from pages.results import GoogleResultsPage

def test_basic_google_search(driver):
    search_page = GoogleSearchPage(driver)
    result_page = GoogleResultsPage(driver)
    TEXT = "Henry Andres Correa Correa"

    # Given the Google home page is displayed
    search_page.load()

    # When the user search for "Henry Correa"
    search_page.search(TEXT)

    # Then the search result tittle contains "Henry Correa"
    assert TEXT in result_page.title()

    # And the search result query is "Henry Correa"
    assert TEXT == result_page.search_input_value()

    # And search result links pertain to "Henry Correa
    titles = result_page.result_link_titles()
    matches = [t for t in titles if TEXT.lower() in t.lower()]
    assert len(matches) > 0

    #raise Exception("Imcomplete Test")