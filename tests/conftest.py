"""
This module contains shared fixtures.
"""

import pytest
import json
from selenium import webdriver

@pytest.fixture
def config(scope='session'):
      with open('config.json') as config_file:
        config=json.load(config_file)

      assert config['browser'] in ['Firefox', 'Chrome', 'Headless Chrome']
      assert isinstance(config['implicit_wait'],int)
      assert config['implicit_wait'] > 0

      return config

@pytest.fixture
def driver(config):

  if config['browser'] == 'Chrome':
    options = webdriver.ChromeOptions()
    options.add_argument("--start-maximized")
    b = webdriver.Chrome(options=options)
  elif config['browser'] == 'Headless Chrome':
    options = webdriver.ChromeOptions()
    options.add_argument("headless")
    b = webdriver.Chrome(options=options)
  elif config['browser'] == 'Firefox':
    b = webdriver.Firefox(s)
  else:
    raise Exception(f'Browser "{config["browser"]}" is no supported')

  # Make its calls wait up to 10 seconds for elements to appear
  b.implicitly_wait(10)

  # Return the WebDriver instance for the setup
  yield b

  # Quit the WebDriver instance for the cleanup
  b.quit()