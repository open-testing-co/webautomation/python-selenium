""" 
This module contains GoogleSearchPage
the page object for the last page
"""
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
class GoogleSearchPage:
    URL = 'https://www.google.com/'
    SEARCH_INPUT = (By.NAME, 'q')
    SEARCH_BUTTON = (By.NAME, 'btnK')

    def __init__(self, driver):
        self.driver = driver

    def load(self):
        self.driver.get(self.URL)

    def search(self, text):
        txt_search = self.driver.find_element(*self.SEARCH_INPUT)
        txt_search.send_keys(text+Keys.RETURN)
