# Python-selenium
## Prerequisites: 
* Python3 Installed ```python --version```
* pip installed ```pip --version```
* pipenv installed ```pipenv --version``` or ```pip install pipenv``` follow the next [guide](https://docs.python-guide.org/dev/virtualenvs/)
* pytest installed ```pytest --version``` or ```pip install -U pytest```

## Steps
### Step 1
Install dependency manager into project path ```pipenv install```

### Step 2
create a ```tests``` directory

### Step 3
Create your first test file with the next format ```test_*.py``` or ```*_test.py```

### Step 4
Add dependency into Pipfile
```
[packages]
pytest = "*"
```

### Step 5
Install selenium dependency
```
pipenv install selenium
```

### Step 6
Install selenium dependency
```
pipenv install pytest-xdist
```
### run
run ```pipenv run python -m pytest```